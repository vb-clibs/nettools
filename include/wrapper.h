#ifndef VB_NETTOOLS_WRAPPER_H
#define VB_NETTOOLS_WRAPPER_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>

/*
 *
 */
int vb_socket(int domain, int type, int protocol);

/*
 *
 */
void vb_bind(int sockfd, struct sockaddr *addr, socklen_t addrlen);

/*
 *
 */
void vb_listen(int sockfd, int backlog);

/*
 *
 */
int vb_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);

/*
 *
 */
void vb_connect(int sockfd, struct sockaddr *addr, socklen_t addrlen);

/*
 *
 */
ssize_t vb_full_read(int fd, void *buff, size_t count);

/*
 *
 */
ssize_t vb_full_write(int fd, void *buff, size_t count);


#ifdef VB_NAMESPACES
struct vb_nettools_namespace {
	int (*socket)(int, int, int);
	void (*bind)(int, struct sockaddr *, socklen_t);
	void (*listen)(int, int backlog);
	int (*accept)(int, struct sockaddr *, socklen_t *);
	void (*connect)(int, struct sockaddr *, socklen_t);
	ssize_t (*read)(int, void *, size_t);
	ssize_t (*write)(int, void *, size_t);
};


static struct vb_nettools_namespace
VB_NetTools = {
	.socket   =  &vb_socket,
	.bind     =  &vb_bind,
	.listen   =  &vb_listen,
	.accept   =  &vb_accept,
	.connect  =  &vb_connect,
	.read     =  &vb_full_read,
	.write    =  &vb_full_write,
};
#endif
#endif /* VB_NETTOOLS_WRAPPER_H */
