#include <wrapper.h>


int vb_socket(int domain, int type, int protocol)
{
    int fd = socket(domain, type, protocol);

    if(fd < 0) {
        perror("unable to create socket");
        exit(EXIT_FAILURE);
    }

    return fd;
}


void vb_connect(int sockfd, struct sockaddr *addr, socklen_t addrlen)
{
    if(connect(sockfd, addr, addrlen) < 0) {
        perror("error on connect");
        exit(EXIT_FAILURE);
    }
}


void vb_bind(int sockfd, struct sockaddr *addr, socklen_t addrlen)
{
    if(bind(sockfd, addr, addrlen) < 0) {
        perror("error on bind");
        exit(EXIT_FAILURE);
    }
}


void vb_listen(int sockfd, int backlog)
{
    if(listen(sockfd, backlog) < 0) {
        perror("error on listen");
        exit(EXIT_FAILURE);
    }
}


int vb_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
    int fd;

    do {
        fd = accept(sockfd, addr, addrlen);
    } while(fd < 0 && errno == EINTR);

    if(fd < 0)
    {
        perror("error on accept");
        exit(EXIT_FAILURE);
    }

    return fd;
}
