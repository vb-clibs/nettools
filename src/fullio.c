#include <wrapper.h>


ssize_t vb_full_write(int fd, void *buff, size_t count)
{
    ssize_t nwritten = 0;
    size_t left = count;

    while(left > 0) {
        nwritten = write(fd, buff, left);

        if(nwritten < 0 && errno != EINTR) {
            return -nwritten;
        }
        else {
            buff = (char *)buff + nwritten;
            left -= nwritten;
        }
    }
    return left;
}


ssize_t vb_full_read(int fd, void *buff, size_t count)
{
    ssize_t nread = 1;
    size_t left = count;

    while(left > 0 && nread != 0) {
        nread = read(fd, buff, left);

        if(nread < 0 && errno != EINTR) {
            return -nread;
        }
        else {
            buff = (char *)buff + nread;
            left -= nread;
        }
    }
    return left;
}

